package libs

import (
	"context"
	"strconv"

	"github.com/mongodb/mongo-go-driver/mongo"
)

func connectToDB() *mongo.Database {
	config := loadConfig("./configs/database.json")
	client, _ := mongo.NewClient("mongodb://" + config.Host + ":" + strconv.Itoa(config.Port))

	client.Connect(context.TODO())
	return client.Database(config.Database)
}

type Collection struct {
	collection string
}

func (nc Collection) insert(data map[string]string) {
	connectToDB().Collection(nc.collection).InsertOne(context.Background(), data)
}
