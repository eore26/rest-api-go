package libs

import (
	"encoding/json"
	"io/ioutil"
)

type config struct {
	Host     string `json:"host"`
	Port     int    `json:"port"`
	Database string `json:"database"`
	Username string `json:"username"`
	Password string `json:"password"`
}

func loadConfig(file string) config {
	f, _ := ioutil.ReadFile(file)
	var i config
	json.Unmarshal(f, &i)
	return i
}
